const rl = @import("raylib");
const std = @import("std");
const patterns = @import("patterns.zig");

var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const allocator = gpa.allocator();

// Some structs.
pub const game_data = struct {
    started: bool,
    entity_list: std.ArrayList(entity),
    bullet_list: std.ArrayList(entity),
    player: ?*entity,
    controls: [256]u8,
};

pub const entity = struct {
    pos: struct { x: i32, y: i32 },
    team: side,
    shooting: bool,
    pattern: ?*const fn (self: *const entity) void,
};

// Some enums.
const side = enum { SIDE_PLAYER, SIDE_ENEMY };

// Zero intiialise the slow way since `zeroes` is apparently bad.
pub var data: game_data = .{
    .started = false,
    .entity_list = std.ArrayList(entity).init(allocator),
    .bullet_list = std.ArrayList(entity).init(allocator),
    .player = null,
    .controls = [_]u8{0} ** 256,
};

// Set everything up for the game.
pub fn init() !void {
    // Start with some Raylib initialisation.
    const screenWidth = 800;
    const screenHeight = 450;

    rl.initWindow(screenWidth, screenHeight, "A Zig Shmup");
    rl.toggleBorderlessWindowed();

    rl.setTargetFPS(60); // Set our game to run at 60 frames-per-second
    // Create the player.
    try data.entity_list.append(.{
        .pos = .{ .x = 300, .y = 300 },
        .team = side.SIDE_PLAYER,
        .shooting = false,
        .pattern = &patterns.player_shot,
    });
    data.player = &data.entity_list.items[0];
}

pub fn handle_input() !void {
    // Use the simplest solution possible: just check each key's down status.
    // An A-press is an A-press. We can't have just half.
    var player_speed: u8 = 4;
    if (rl.isKeyDown(rl.KeyboardKey.key_left_shift)) {
        player_speed -= 2;
    }
    if (rl.isKeyDown(rl.KeyboardKey.key_down)) {
        data.player.?.*.pos.y += player_speed;
    }
    if (rl.isKeyDown(rl.KeyboardKey.key_up)) {
        data.player.?.*.pos.y -= player_speed;
    }
    if (rl.isKeyDown(rl.KeyboardKey.key_left)) {
        data.player.?.*.pos.x -= player_speed;
    }
    if (rl.isKeyDown(rl.KeyboardKey.key_right)) {
        data.player.?.*.pos.x += player_speed;
    }
    // Shooting is a mode. We'll handle it as part of stage logic for each entity,
    // so just set it.
    data.player.?.*.shooting = rl.isKeyDown(rl.KeyboardKey.key_z);
}

pub fn logic() !void {
    try handle_input();
    // Shoot.
    for (data.entity_list.items) |e| {
        if (e.shooting) {
            e.pattern.?(&e);
        }
    }
    // Move bullets.
    for (data.bullet_list.items, 0..) |e, idx| {
        if (e.team == side.SIDE_PLAYER) {
            data.bullet_list.items[idx].pos.y -= 4;
        }
    }
    //const current_tick: f64 = rl.getTime();
    //std.debug.print("{}\n", .{current_tick});
}

pub fn draw() !void {
    //const current_tick: f64 = rl.getTime();
    rl.drawText("Congrats! You created your first window!", 190, 200, 20, rl.Color.light_gray);
    const out_str = try std.fmt.allocPrint(allocator, "Current time: {}" ++ [_]u8{0}, .{rl.getTime()});
    defer allocator.free(out_str);
    rl.drawText(@ptrCast(out_str), 190, 250, 20, rl.Color.dark_blue);
    const out_col = switch (data.player.?.*.shooting) {
        false => rl.Color.red,
        true => rl.Color.blue,
    };
    rl.drawCircle(data.player.?.*.pos.x, data.player.?.*.pos.y, 3, out_col);
    for (data.bullet_list.items) |e| {
        rl.drawCircle(e.pos.x, e.pos.y, 2, out_col);
    }
}

pub fn cleanup() !void {
    data.entity_list.deinit();
}
