const std = @import("std");
const game = @import("game.zig");

fn shoot_straight(self: *const game.entity, speed: u8, angle: f32) void {
    _ = angle;
    _ = speed;
    game.data.bullet_list.append(.{
        .pos = self.*.pos,
        .team = self.*.team,
        .shooting = false,
        .pattern = null,
    }) catch return;
}

/// The player's shot. For now, a basic stream of upward triangles.
pub fn player_shot(self: *const game.entity) void {
    //std.debug.print("{}\n", .{self.shooting});
    shoot_straight(self, 0, 0);
    return;
}
